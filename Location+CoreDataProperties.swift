//
//  Location+CoreDataProperties.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 07/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation
import CoreData

extension Location {

    @NSManaged var locationId: String?
    @NSManaged var address: String?

}
