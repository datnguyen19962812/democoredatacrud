//
//  EntityTypes.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 07/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation

enum EntityTypes: String {
    case Event = "Event"
    static let getAll = [Event]
}
