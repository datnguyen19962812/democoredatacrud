//
//  RemoteReplicator.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 07/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation

class RemoteReplicator: ReplicatorProtocol {

    fileprivate var eventAPI: EventAPI!
    fileprivate var httpClient: HTTPClient!

    class var sharedInstance: RemoteReplicator {
        struct Singleton {
            static let instance = RemoteReplicator()
        }

        return Singleton.instance
    }

    init() {
        self.eventAPI = EventAPI.sharedInstance
        self.httpClient = HTTPClient()
    }

    func fetchData() {

        //Remote resource
        let request: URLRequest = URLRequest(url: URL(string: "https://www.dropbox.com/s/mq5o0f4fiyl0hwc/remote_events.json?dl=1")!)

        httpClient.doGet(request) { (data, _, httpStatusCode) -> Void in
            if httpStatusCode!.rawValue != HTTPStatusCode.ok.rawValue {
                print("\(httpStatusCode!.rawValue) \(String(describing: httpStatusCode))")
                if data == nil {
                    print("data is nil")
                }
            } else {
                DispatchQueue.global().async {
                    self.processData(data! as AnyObject?)
                    DispatchQueue.main.async {
                        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateEventTableData"), object: nil)
                    }
                }
            }
        }
    }

    internal func processData(_ jsonResponse: AnyObject?) {

        let jsonData: Data = jsonResponse as! Data
        var jsonResult: AnyObject!

        do {
            jsonResult = try JSONSerialization.jsonObject(with: jsonData) as AnyObject
        } catch let fetchError as NSError {
            print("pull error: \(fetchError.localizedDescription)")
        }

        var retrievedEvents: [Dictionary<String, AnyObject>] = []

        if let eventList = jsonResult {
            for index in 0..<eventList.count {
                var eventItem: Dictionary<String, AnyObject> = eventList[index] as! Dictionary<String, AnyObject>
                eventItem[EventAttributes.title.rawValue] = "[REMOTE] \(eventItem[EventAttributes.title.rawValue]!)" as AnyObject?
                eventItem[EventAttributes.eventId.rawValue] = UUID().uuidString as AnyObject?
                eventItem[EventAttributes.attendees.rawValue] = AttendeesGenerator.getSemiRandomGeneratedAttendeesList() as AnyObject?

                retrievedEvents.append(eventItem)
            }
        }
        eventAPI.saveEventsList(retrievedEvents as Array<AnyObject>)
    }
}
