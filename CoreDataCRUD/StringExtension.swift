//
//  StringExtension.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation

extension String {

    mutating func append(_ str: String) {
        self = self + str
    }

    mutating func URLEncodedString() -> String? {
        let customAllowedSet =  CharacterSet.urlQueryAllowed
        let escapedString = self.addingPercentEncoding(withAllowedCharacters: customAllowedSet)

        return escapedString
    }
}
