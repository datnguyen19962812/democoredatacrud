//
//  EventTableViewController.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import UIKit

class EventTableViewController: UITableViewController, UISearchResultsUpdating {

    fileprivate var eventList: Array<Event> = []
    fileprivate var filteredEventList: Array<Event> = []
    fileprivate var selectedEventItem: Event!
    fileprivate var resultSearchController: UISearchController!
    fileprivate var eventAPI: EventAPI!
    fileprivate let eventTableCellIdentifier = "eventItemCell"
    fileprivate let showEventItemSegueIdentifier = "showEventItemSegue"
    fileprivate let editEventItemSegueIdentifier = "editEventItemSegue"

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    override func viewDidLoad() {
        super.viewDidLoad()
        initResultSearchController()
    }

    override func viewWillAppear(_ animated: Bool) {
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.updateEventTableData), name: .updateEventTableData, object: nil)
        notificationCenter.addObserver(self, selector: #selector(self.setStateLoading), name: .setStateLoading, object: nil)

        self.eventAPI = EventAPI.sharedInstance
        self.eventList = self.eventAPI.getEventsInDateRange()
        self.title = String(format: "Upcoming events (%i)", eventList.count)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if resultSearchController.isActive {
            return self.filteredEventList.count
        }

        return eventList.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let eventCell =
        tableView.dequeueReusableCell(withIdentifier: eventTableCellIdentifier, for: indexPath) as! EventTableViewCell

        let eventItem: Event!

        if resultSearchController.isActive {
            eventItem = filteredEventList[(indexPath as NSIndexPath).row]
        } else {
            eventItem = eventList[(indexPath as NSIndexPath).row]
        }

        eventCell.eventDateLabel.text = DateFormatter.getStringFromDate(eventItem.date, dateFormat: "dd-MM\nyyyy")
        eventCell.eventTitleLabel.text = eventItem.title
        eventCell.eventLocationLabel.text = "\(eventItem.venue) - \(eventItem.city) - \(eventItem.country)"
        eventCell.eventImageView.image = self.getEventImage(indexPath)
        
        return eventCell
    }

    // MARK: - Navigation

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        let destination = segue.destination as? EventItemViewController

        if segue.identifier == showEventItemSegueIdentifier {

            let selectedEventItem: Event!

            if resultSearchController.isActive {
                selectedEventItem = filteredEventList[(self.tableView.indexPathForSelectedRow! as NSIndexPath).row] as Event
                resultSearchController.isActive = false
            } else {
                selectedEventItem = eventList[(self.tableView.indexPathForSelectedRow! as NSIndexPath).row] as Event
            }

            destination!.selectedEventItem = eventAPI.getEventById(selectedEventItem.eventId as NSString)[0] //option 2

            destination!.title = "Edit event"
        } else if segue.identifier == editEventItemSegueIdentifier {
            destination!.title = "Add event"
        }
    }


    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            eventAPI.deleteEvent(eventList[(indexPath as NSIndexPath).row])
            self.title = String(format: "Upcoming events (%i)", eventList.count)
        }
    }

    func updateSearchResults(for searchController: UISearchController) {
        filterEventListContent(searchController.searchBar.text!)
        refreshTableData()
    }

    fileprivate func initResultSearchController() {
        resultSearchController = UISearchController(searchResultsController: nil)
        resultSearchController.searchResultsUpdater = self
        resultSearchController.dimsBackgroundDuringPresentation = false
        resultSearchController.searchBar.sizeToFit()

        self.tableView.tableHeaderView = resultSearchController.searchBar
    }

    fileprivate func filterEventListContent(_ searchTerm: String) {
        filteredEventList.removeAll(keepingCapacity: false)
        var predicates = [NSPredicate]()
        predicates.append(NSPredicate(format: "\(EventAttributes.title.rawValue) contains[cd] %@", searchTerm.lowercased()))
        predicates.append(NSPredicate(format: "%K contains[c] %@", #keyPath(Event.country), searchTerm.lowercased()))
        predicates.append(NSPredicate(format: "\(EventAttributes.city.rawValue)  contains[c] %@", searchTerm.lowercased()))
        let compoundPredicate = NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
        filteredEventList =  eventList.filter {compoundPredicate.evaluate(with: $0)}
    }

    @objc func updateEventTableData() {
        refreshTableData()
        self.activityIndicator.isHidden = true
        self.activityIndicator.stopAnimating()
    }

    @objc func setStateLoading() {
        refreshTableData()
        self.activityIndicator.isHidden = false
        self.activityIndicator.startAnimating()
    }
    fileprivate func refreshTableData() {
        self.eventList.removeAll(keepingCapacity: false)
        self.eventList = self.eventAPI.getEventsInDateRange()
        self.tableView.reloadData()
        self.title = String(format: "Upcoming events (%i)", self.eventList.count)
    }

    fileprivate func getEventImage(_ indexPath: IndexPath) -> UIImage {
        return UIImage(named: "eventImageSecond.jpg")!
    }
}

extension Notification.Name {
    static let updateEventTableData = Notification.Name(rawValue: "updateEventTableData")
    static let setStateLoading = Notification.Name(rawValue: "setStateLoading")
}
