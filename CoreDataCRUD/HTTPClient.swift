//
//  HTTPClient.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation

class HTTPClient {

    fileprivate var urlSession: URLSession!
    fileprivate var sessionConfiguration: URLSessionConfiguration!
    init() {
        sessionConfiguration = URLSessionConfiguration.default
        urlSession = URLSession(configuration: sessionConfiguration)
    }

    func setAdditionalHeaders(_ headers: Dictionary<String, AnyObject>) {
        sessionConfiguration.httpAdditionalHeaders = headers
    }

    func queryBuilder(_ params: Dictionary<String, AnyObject>) -> String {

        var queryString: String = "?"
        var counter = 0

        for (key, value) in params {

            queryString.append("\(key)=\(value)")

            if params.count > 1  && counter != params.count {
                queryString.append("&")
            }

            counter += 1
        }

        return queryString
    }

    func doGet(_ request: URLRequest!, callback:@escaping (_ data: Data?, _ error: NSError?, _ httpStatusCode: HTTPStatusCode?) -> Void) {
        let task = urlSession.dataTask(with: request, completionHandler: {
            (data, response, error) -> Void in
            if let responseError = error {
                callback(nil, responseError as NSError?, nil)
            } else if let httpResponse = response as? HTTPURLResponse {

                let httpStatus = self.getHTTPStatusCode(httpResponse)
                print("HTTP Status Code: \(httpStatus.rawValue) \(httpStatus)")

                if httpStatus.rawValue != 200 {
                    let statusError = NSError(domain:"com.example.CoreDataCRUD", code:httpStatus.rawValue, userInfo:[NSLocalizedDescriptionKey: "HTTP status code: \(httpStatus.rawValue) - \(httpStatus)"])
                    callback(nil, statusError, httpStatus)
                } else {
                    callback(data, nil, httpStatus)
                }

            }
        })

        task.resume()
    }

    func getHTTPStatusCode(_ httpURLResponse: HTTPURLResponse) -> HTTPStatusCode {
        var httpStatusCode: HTTPStatusCode!

        for status in HTTPStatusCode.getAll {
            if httpURLResponse.statusCode == status.rawValue {
                httpStatusCode = status
            }
        }

        return httpStatusCode
    }

}
