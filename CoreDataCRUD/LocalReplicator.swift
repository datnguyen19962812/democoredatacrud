//
//  Replicator.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation

class LocalReplicator: ReplicatorProtocol {

    fileprivate var eventAPI: EventAPI!

    class var sharedInstance: LocalReplicator {
        struct Singleton {
            static let instance = LocalReplicator()
        }

        return Singleton.instance
    }

    init() {
        self.eventAPI = EventAPI.sharedInstance
    }

    func fetchData() {
        
        DispatchQueue.global(qos: .background).async {
            
            self.processData(self.readFile() as AnyObject?)
            DispatchQueue.main.async {
                NotificationCenter.default.post(name: Notification.Name(rawValue: "updateEventTableData"), object: nil)
            }

        }
    }

    func readFile() -> Any? {
        let dataSourceFilename: String = "events"
        let dataSourceFilenameExtension: String = "json"
        let filemgr = FileManager.default
        let currPath = Bundle.main.path(forResource: dataSourceFilename, ofType: dataSourceFilenameExtension)
        var jsonResult:Any? = nil

        do {
            let jsonData: Data = try! Data(contentsOf: URL(fileURLWithPath: currPath!))

            if filemgr.fileExists(atPath: currPath!) {
                jsonResult = try JSONSerialization.jsonObject(with: jsonData)
            } else {
                print("\(dataSourceFilename).\(dataSourceFilenameExtension)) does not exist, therefore cannot read JSON data.")
            }
        } catch let fetchError as NSError {
            print("read file error: \(fetchError.localizedDescription)")
        }

        return jsonResult
    }


    func processData(_ jsonResult: AnyObject?) {
        var retrievedEvents = [Dictionary<String, AnyObject>]()

        if let eventList = jsonResult {
            for index in 0..<eventList.count {
                var eventItem: Dictionary<String, AnyObject> = eventList[index] as! Dictionary<String, AnyObject>
                eventItem[EventAttributes.title.rawValue] = "[LOCAL] \(eventItem[EventAttributes.title.rawValue]!)"  as AnyObject?
                eventItem[EventAttributes.eventId.rawValue] = UUID().uuidString as AnyObject?
                eventItem[EventAttributes.attendees.rawValue] = NSSet.init(array: (eventItem[EventAttributes.attendees.rawValue] as! [Any]))
                NSSet.init(array: [])
                
//                NSSet *telephoneSet = [[NSSet alloc] init];
//                [telephoneSet setByAddingObjectsFromArray:telephoneArray];
                retrievedEvents.append(eventItem)
            }
        }
        eventAPI.saveEventsList(retrievedEvents as Array<AnyObject>)
    }
}
