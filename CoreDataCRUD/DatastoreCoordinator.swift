//
//  DatastoreCoordinator.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 07/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//
import Foundation
import CoreData

class DatastoreCoordinator: NSObject {

    fileprivate let objectModelName = "CoreDataCRUD"
    fileprivate let objectModelExtension = "momd"
    fileprivate let dbFilename = "SingleViewCoreData.sqlite"
    fileprivate let appDomain = "com.example.CoreDataCRUD"

    override init() {
        super.init()
    }

    lazy var applicationDocumentsDirectory: URL = {
        let urls = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)

        return urls[urls.count-1]
    }()

    
    lazy var managedObjectModel: NSManagedObjectModel = {
        let modelURL = Bundle.main.url(forResource: self.objectModelName, withExtension: self.objectModelExtension)!

        return NSManagedObjectModel(contentsOf: modelURL)!
    }()

    
    lazy var persistentStoreCoordinator: NSPersistentStoreCoordinator = {

        // The persistent store coordinator for the application. This implementation creates and return a coordinator,
        // having added the store for the application to it. This property is optional since there are legitimate error
        // conditions that could cause the creation of the store to fail.

        // Create the coordinator and store
        let coordinator = NSPersistentStoreCoordinator(managedObjectModel: self.managedObjectModel)
        let url = self.applicationDocumentsDirectory.appendingPathComponent(self.dbFilename)
        var failureReason = "There was an error creating or loading the application's saved data."

        do {
            try coordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
        } catch {
            
            var dict = [String: AnyObject]()
            dict[NSLocalizedDescriptionKey] = "Failed to initialize the application's saved data" as AnyObject?
            dict[NSLocalizedFailureReasonErrorKey] = failureReason as AnyObject?

            dict[NSUnderlyingErrorKey] = error as NSError
            let wrappedError = NSError(domain: self.appDomain, code: 9999, userInfo: dict)
            
            NSLog("Unresolved error \(wrappedError), \(wrappedError.userInfo)")

            abort()
        }

        return coordinator
    }()
}
