//
//  EventAPI.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import UIKit
import CoreData

class EventAPI {

    fileprivate let persistenceManager: PersistenceManager!
    fileprivate var mainContextInstance: NSManagedObjectContext!

    fileprivate let idNamespace = EventAttributes.eventId.rawValue
    fileprivate let fbURLNamespace = EventAttributes.fb_url.rawValue
    fileprivate let ticketURLNamespace = EventAttributes.ticket_url.rawValue
    fileprivate let titleNamespace = EventAttributes.title.rawValue
    fileprivate let dateNamespace = EventAttributes.date.rawValue
    fileprivate let cityNamespace = EventAttributes.city.rawValue
    fileprivate let venueNamespace = EventAttributes.venue.rawValue
    fileprivate let countryNamespace = EventAttributes.country.rawValue
    fileprivate let attendeesNamespace = EventAttributes.attendees.rawValue


    class var sharedInstance: EventAPI {
        struct Singleton {
            static let instance = EventAPI()
        }

        return Singleton.instance
    }

    init() {
        self.persistenceManager = PersistenceManager.sharedInstance
        self.mainContextInstance = persistenceManager.getMainContextInstance()
    }


    func getSingleAndOnlyEvent(eventTitle: String) -> Event? {
        var fetchedResultEvent: Event?
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityTypes.Event.rawValue)
        do {
            let fetchedResults = try  self.mainContextInstance.fetch(fetchRequest) as! [Event]
            fetchRequest.fetchLimit = 1
            
            if fetchedResults.count != 0 {
                fetchedResultEvent =  fetchedResults.first
            }
        } catch let fetchError as NSError {
            print("retrieve single event error: \(fetchError.localizedDescription)")
        }
        
        return fetchedResultEvent
    }
    
    func saveEvent(_ eventDetails: Dictionary<String, AnyObject>) {
        let minionManagedObjectContextWorker: NSManagedObjectContext =
        NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        minionManagedObjectContextWorker.parent = self.mainContextInstance
        let eventItem = NSEntityDescription.insertNewObject(forEntityName: EntityTypes.Event.rawValue,
            into: minionManagedObjectContextWorker) as! Event
        for (key, value) in eventDetails {
            for attribute in EventAttributes.getAll {
                if (key == attribute.rawValue) {
                    eventItem.setValue(value, forKey: key)
                }
            }
        }
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        self.persistenceManager.mergeWithMainContext()
        self.postUpdateNotification()
    }
    

    func saveEvent(_ eventTitle: String?) {
        let minionManagedObjectContextWorker: NSManagedObjectContext =
            NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        minionManagedObjectContextWorker.parent = self.mainContextInstance
        let eventItem = NSEntityDescription.insertNewObject(forEntityName: EntityTypes.Event.rawValue,
                                                            into: minionManagedObjectContextWorker) as! Event
        if eventTitle == nil {
            eventItem.title = "TITLE"
        } else {
            eventItem.title = eventTitle!
            eventItem.city = "Nam Dinh"
            eventItem.country = "Viet Nam"
            eventItem.fb_url =  URL(string: "http://example.com") as AnyObject
            eventItem.ticket_url = URL(string: "http://example.com") as AnyObject
            eventItem.date = Date()
            eventItem.eventId = "1234"
        }
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        self.persistenceManager.mergeWithMainContext()
        self.postUpdateNotification()
    }

    func saveEventsList(_ eventsList: Array<AnyObject>) {
        DispatchQueue.global().async {
            let minionManagedObjectContextWorker: NSManagedObjectContext =
                NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
            minionManagedObjectContextWorker.parent = self.mainContextInstance
            for index in 0..<eventsList.count {
                var eventItem: Dictionary<String, NSObject> = eventsList[index] as! Dictionary<String, NSObject>
                if eventItem[self.dateNamespace] as! String != ""
                    && eventItem[self.titleNamespace] as! String != ""
                    && eventItem[self.cityNamespace] as! String != "" {
                    let item = NSEntityDescription.insertNewObject(forEntityName: EntityTypes.Event.rawValue,
                                                                   into: minionManagedObjectContextWorker) as! Event
                    item.setValue(DateFormatter.getDateFromString(eventItem[self.dateNamespace] as! String), forKey: self.dateNamespace)
                    item.setValue(eventItem[self.titleNamespace], forKey: self.titleNamespace)
                    item.setValue(eventItem[self.cityNamespace], forKey: self.cityNamespace)
                    item.setValue(eventItem[self.venueNamespace], forKey: self.venueNamespace)
                    item.setValue(eventItem[self.countryNamespace], forKey: self.countryNamespace)
                    item.setValue(eventItem[self.idNamespace], forKey: self.idNamespace)
                    item.setValue(eventItem[self.fbURLNamespace], forKey: self.fbURLNamespace)
                    item.setValue(eventItem[self.ticketURLNamespace], forKey: self.ticketURLNamespace)
                    item.setValue(eventItem[self.attendeesNamespace], forKey: self.attendeesNamespace)
                    self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
                }
            }
            self.persistenceManager.mergeWithMainContext()
            DispatchQueue.main.async {
                self.postUpdateNotification()
            }
        }
    }

    func getAllEvents(_ sortedByDate: Bool = true, sortAscending: Bool = true) -> Array<Event> {
        var fetchedResults: Array<Event> = Array<Event>()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityTypes.Event.rawValue)
        if sortedByDate {
            let sortDescriptor = NSSortDescriptor(key: dateNamespace,
                ascending: sortAscending)
            let sortDescriptors = [sortDescriptor]
            fetchRequest.sortDescriptors = sortDescriptors
        }
        do {
            fetchedResults = try  self.mainContextInstance.fetch(fetchRequest) as! [Event]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Event>()
        }

        return fetchedResults
    }

    func getEventById(_ eventId: NSString) -> Array<Event> {
        var fetchedResults: Array<Event> = Array<Event>()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityTypes.Event.rawValue)
        let findByIdPredicate =
        NSPredicate(format: "\(idNamespace) = %@", eventId)
        fetchRequest.predicate = findByIdPredicate
        do {
            fetchedResults = try self.mainContextInstance.fetch(fetchRequest) as! [Event]
        } catch let fetchError as NSError {
            print("retrieveById error: \(fetchError.localizedDescription)")
            fetchedResults = Array<Event>()
        }

        return fetchedResults
    }

    
    
    func getEventsInDateRange(_ sortByDate: Bool = true, sortAscending: Bool = true,
        startDate: Date = Date(timeInterval:-189216000, since:Date()),
        endDate: Date = (Calendar.current as NSCalendar)
            .date(
                byAdding: .day, value: 7,
                to: Date(),
                options: NSCalendar.Options(rawValue: 0))!) -> Array<Event> {
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: EntityTypes.Event.rawValue)
        let sortDescriptor = NSSortDescriptor(key: EventAttributes.date.rawValue,
            ascending: sortAscending)
        let sortDescriptors = [sortDescriptor]
        fetchRequest.sortDescriptors = sortDescriptors
        let findByDateRangePredicate = NSPredicate(format: "(\(dateNamespace) >= %@) AND (\(dateNamespace) <= %@)", startDate as CVarArg, endDate as CVarArg)
        fetchRequest.predicate = findByDateRangePredicate
        var fetchedResults = Array<Event>()
        do {
            fetchedResults = try self.mainContextInstance.fetch(fetchRequest) as! [Event]
        } catch let fetchError as NSError {
            print("retrieveItemsSortedByDateInDateRange error: \(fetchError.localizedDescription)")
        }

        return fetchedResults
    }

    func updateEvent(_ eventItemToUpdate: Event, newEventItemDetails: Dictionary<String, AnyObject>) {

        let minionManagedObjectContextWorker: NSManagedObjectContext =
        NSManagedObjectContext.init(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
        minionManagedObjectContextWorker.parent = self.mainContextInstance

        //Assign field values
        for (key, value) in newEventItemDetails {
            for attribute in EventAttributes.getAll {
                if (key == attribute.rawValue) {
                    eventItemToUpdate.setValue(value, forKey: key)
                }
            }
        }
        self.persistenceManager.saveWorkerContext(minionManagedObjectContextWorker)
        self.persistenceManager.mergeWithMainContext()

        self.postUpdateNotification()
    }
    
    func deleteAllEvents() {
        let retrievedItems = getAllEvents()
        for item in retrievedItems {
            self.mainContextInstance.delete(item)
        }
        self.persistenceManager.mergeWithMainContext()
        self.postUpdateNotification()
    }
    func deleteEvent(_ eventItem: Event) {
        print(eventItem)
        self.mainContextInstance.delete(eventItem)
        self.persistenceManager.mergeWithMainContext()
        self.postUpdateNotification()
    }

    fileprivate func postUpdateNotification() {
        NotificationCenter.default.post(name: Notification.Name(rawValue: "updateEventTableData"), object: nil)
    }

}
