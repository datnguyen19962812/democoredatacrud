//
//  AppDelegate.swift
//  CoreDataCRUD


import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    fileprivate var eventAPI: EventAPI!
    fileprivate var localReplicator: LocalReplicator!
    fileprivate let runCountNamespace = "runCount"

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.eventAPI = EventAPI.sharedInstance
        self.localReplicator = LocalReplicator.sharedInstance
        self.handleRunCount()

        return true
    }

    func sharedInstance() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }

    lazy var datastoreCoordinator: DatastoreCoordinator = {
        return DatastoreCoordinator()
    }()

    lazy var contextManager: ContextManager = {
        return ContextManager()
    }()

    fileprivate func handleRunCount() {
        let defaults = UserDefaults.standard
        var runCount: Int = defaults.integer(forKey: runCountNamespace)

        if(runCount == 0) {
            localReplicator.fetchData()
        }

        runCount += 1
        defaults.set(runCount, forKey:runCountNamespace)
        print("current runCount: \(runCount)")
    }

}
