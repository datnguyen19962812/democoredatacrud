//
//  DateFormatter.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation

class DateFormatter {


    class func getDateFromString(_ dateString: String, dateFormat: String = "dd-MM-yyyy") -> Date {
        let dateFormatter = Foundation.DateFormatter()
        dateFormatter.dateFormat = dateFormat

        return dateFormatter.date(from: dateString)!
    }

    class func getStringFromDate(_ date: Date, dateFormat: String = "dd-MM-yyyy") -> String {
        let dateFormatter = Foundation.DateFormatter()
        dateFormatter.dateFormat = dateFormat

        return dateFormatter.string(from: date)
    }

}
