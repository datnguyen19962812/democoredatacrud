//
//  AttendeesGenerator.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation

class AttendeesGenerator {

    class func getSemiRandomGeneratedAttendeesList() -> Array<String> {
        let optionalAttendees = [
            "Ronaldo",
            "Tevez",
            "Messi",
            "Aguaro",
            "Dat",
            "Duc",
            "Phuc",
            "Narcissus",
            "Frodo",
            "Esscher",
            "Lothar Collatz",
            "Foo",
            "Bar",
            "Tweety"
        ]

        var attendeesList = [String]()
        let listSize: Int =  3

        for _ in 0...listSize {
            let itemIndex: Int =  0
            let item: String = optionalAttendees[itemIndex]
            if !attendeesList.contains(item) {
                attendeesList.append(item)
            }
        }

        return attendeesList
    }

}
