//
//  Event.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 07/03/2022.
//  Copyright © 2022 storm. All rights reserved.
//

import Foundation
import CoreData



@objc(Event)
class Event: NSManagedObject {
    @NSManaged var city: String
    @NSManaged var country: String
    @NSManaged var venue: String
    @NSManaged var eventId: String
    @NSManaged var date: Date
    @NSManaged var fb_url: AnyObject
    @NSManaged var ticket_url: AnyObject
    @NSManaged var title: String
    @NSManaged var attendees: NSSet
}
