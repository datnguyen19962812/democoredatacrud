//
//  Attendee.swift
//  CoreDataCRUD
//
//  Created by Nguyen Dat on 14/03/2022.
//  Copyright © 2022 io pandacode. All rights reserved.
//

import Foundation
import CoreData

@objc(Attendee)
class Attendee: NSManagedObject {
    @NSManaged var dateOfbirth: String
    @NSManaged var id: String
    @NSManaged var firstName: String
    @NSManaged var lastName: String
    @NSManaged var event: Event
}
